import React, {
  Fragment,
  useEffect,
  useState,
  useCallback,
  useMemo,
} from "react";
import axios from "axios";
import swal from "sweetalert";
import { useDropzone } from "react-dropzone";
import { environment } from "../environment";
import {
  Avatar,
  Box,
  Button,
  Grid,
  IconButton,
  Input,
} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Dropzone from "react-dropzone";

const baseStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  transition: "border .3s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

export default function UploadSong({ history }) {
  const fileTypes = [".mp3,audio/*"];
  const [songData, setSongsData] = useState([]);
  const [isDataLoaded, setIsDataLoaded] = useState(false);
  const [trackName, setTrackName] = useState("");

  const [files, setFiles] = useState([]);

  const onDrop = useCallback((acceptedFiles) => {
    console.log(acceptedFiles);
    setFiles(
      acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
        })
      )
    );
    onFileInput(acceptedFiles);
  }, []);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    onDrop,
    accept: ".mp3,audio/*",
  });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  const thumbs = files.map((file) => (
    <div key={file.name}>
      {/* <img
          src={file.preview}
          alt={file.name}
        /> */}
      <p>{file.name}</p>
    </div>
  ));
  let uploadResponse;
  let filename;
  let userData;

  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );
  const [file, setFile] = useState(null);
  const handleChange = (file) => {
    setFile(file);
  };
  const getUserDetails = async () => {
    // e.preventDefault()

    // console.log(userDetails);
    await axios
      .get(
        environment.apiUrl + "songsApi/getTracksById/61af22f7c0cc842600f77e42"
      )
      .then((res) => {
        console.log(res);
        // res?.data?.forEach(element => {
        //   element.url = environment.apiUrl+url
        // });
        console.log(res.data);
        setSongsData(res.data);
        setIsDataLoaded(true);
        // setEmail(res.data.email);
        // setAddress(res.data.address);
        // setMobile(res.data.phone);
        // setProfilePicUrl(res.data.profileImageUrl);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFileInput = async (file) => {
    filename = file[0].path;
    console.log(filename);
    setTrackName(filename);
    const formData = new FormData();
    formData.append("userTrack", file[0]);
    await axios
      .post(environment.apiUrl + "fileUploadAPI/uploadFile", formData)
      .then((res) => {
        console.log(res);
        // res?.data?.forEach(element => {
        //   element.url = environment.apiUrl+url
        // });
        console.log(res?.data);
        uploadResponse = res?.data?.success;

        //  setIsDataLoaded(true)
        // setEmail(res.data.email);
        // setAddress(res.data.address);
        // setMobile(res.data.phone);
        // setProfilePicUrl(res.data.profileImageUrl);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const upload = async (e) => {
    userData = JSON.parse(localStorage.getItem("userInfo"));
    console.log(userData);
    console.log(userData?._id);

    await axios
      .post(environment.apiUrl + "api/songs/songsApi/createTrackForUser", {
        url: uploadResponse,
        cover: "",
        title: trackName,
        artist: "Various",
        userId: userData?._id,
      })
      .then((res) => {
        console.log(res);
        // res?.data?.forEach(element => {
        //   element.url = environment.apiUrl+url
        // });
        console.log(res?.data);
        if (res.data) {
          swal({
            title: "success",
            text: "Saved Successfully.",
            icon: "success",
            dangerMode: false,
          });
        }
        //  setIsDataLoaded(true)
        // setEmail(res.data.email);
        // setAddress(res.data.address);
        // setMobile(res.data.phone);
        // setProfilePicUrl(res.data.profileImageUrl);
      })
      .catch((err) => {
        swal({
          title: "Failure",
          text: "Something Wrong Happened",
          icon: "warning",
          dangerMode: true,
        });

        console.log(err);
      });
  };

  const gotoplayer = () => {
    history.push("/musicplayer");
  };

  return (
    <>
      {/* <div className="table-responsive">
                <table className="table table-striped table-hover text-nowrap mb-0">
                    <thead className="thead-light">
                        <tr>
                            <th style={{ width: '40%' }}>Songs</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div className="d-flex align-items-center">
                                    <Avatar alt="..." src="/assets/images/avatars/avatar1.jpg" className="mr-2" />

                                    <div>
                                        <a href="#/" onClick={e => e.preventDefault()} className="font-weight-bold text-black" title="...">Shanelle
                                            Wynn</a>
                                        <span className="text-black-50 d-block">UI Engineer, Apple Inc.</span>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div className="d-flex align-items-center">
                                    <Avatar alt="..." src="/assets/images/avatars/avatar1.jpg" className="mr-2" />
                                    <div>
                                        <a href="#/" onClick={e => e.preventDefault()} className="font-weight-bold text-black" title="...">Beck
                                            Simpson</a>
                                        <span className="text-black-50 d-block">Frontend Developer</span>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <div className="d-flex align-items-center">
                                    <Avatar alt="..." src="/assets/images/avatars/avatar1.jpg" className="mr-2" />

                                    <div>
                                        <a href="#/" onClick={e => e.preventDefault()} className="font-weight-bold text-black" title="...">Regan
                                            Norris</a>
                                        <span className="text-black-50 d-block">Senior Project Manager</span>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div> */}
      {/* <PageTitle
        titleHeading="Default"
        titleDescription="This is a dashboard page example built using this template."
      />

      <DashboardDefaultSection1 />
      <DashboardDefaultSection2 />
      <DashboardDefaultSection3 />
      <DashboardDefaultSection4 /> */}

      <section style={{ marginTop: "50px" }}>
        <div {...getRootProps({ style })}>
          <input {...getInputProps()} />
          <div>Drag and drop your songs here.</div>
        </div>
        <aside>{thumbs}</aside>
      </section>

      <Fragment>
        <Grid item xs={12} lg={12} className='text-center'>
          {/* <p>{file ? `File name: ${file.name}` : "no files uploaded yet"}</p>   */}
        </Grid>

        <Button
          className='m-2'
          variant='contained'
          color='primary'
          onClick={upload}
          style={{ textAlign: "center" }}
        >
          Upload
        </Button>
      </Fragment>

      <div style={{ marginTop: "50px" }}>
        <Button
          className='m-2'
          variant='contained'
          //color='primary'
          onClick={gotoplayer}
          style={{ textAlign: "center" }}
        >
          Go To Music Player
        </Button>
      </div>

      {/* <DropZoneComponent/> */}
    </>
  );
}
