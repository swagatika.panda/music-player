import React, { useState, useEffect } from "react";

import { environment } from "../../environment.js";
import axios from "axios";
import { Avatar, Box, Button, IconButton, Input } from "@material-ui/core";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import "./player.css";
import Player from "../Player";

let songsjson = [];
songsjson = require("./Songs.js");

function MusicPlayer() {
  const [songData, setSongsData] = useState([]);
  const [isDataLoaded, setIsDataLoaded] = useState(false);
  const [isArrayEmpty, setIsArrayEmpty] = useState(true);
  const [currentSongIndex, setCurrentSongIndex] = useState(0);
  const [nextSongIndex, setNextSongIndex] = useState(currentSongIndex + 1);
  const [songdata, setsong] = useState([]);
  let userData;
  useEffect(() => {
    setNextSongIndex(() => {
      if (currentSongIndex + 1 > songs.length - 1) {
        return 0;
      } else {
        return currentSongIndex + 1;
      }
    });

    console.log(songsjson);
    console.log(typeof songsjson);
    userData = JSON.parse(localStorage.getItem("userInfo"));

    // Object.keys(songsjson).map((obj) => {
    //   console.log(obj);
    //   // setsong({
    //   //   title: object.title,
    //   //   artist: object.artist,
    //   //   album: object.album,
    //   //   track: object.track,
    //   //   img_src: object.img_src,
    //   //   src: object.src,
    //   // });
    // });

    console.log(songsjson.arr);

    Object.keys(songsjson.arr).forEach((element) => {
      console.log(songsjson.arr[element].title);
      //setsongs;
    });

    getUserDetails();
  }, [currentSongIndex]);

  const getUserDetails = async () => {
    // e.preventDefault()

    // console.log(userDetails);
    await axios
      .get(
        environment.apiUrl + "api/songs/songsApi/getTracksById/" + userData?._id
      )
      .then((res) => {
        console.log(res);
        // res?.data?.forEach(element => {
        //   element.url = environment.apiUrl+url
        // });
        console.log(res.data);
        setSongsData(res.data);
        if (!res.data.length) {
          setIsArrayEmpty(false);
        } else {
          setIsArrayEmpty(true);
        }
        setIsDataLoaded(true);
        // setEmail(res.data.email);
        // setAddress(res.data.address);
        // setMobile(res.data.phone);
        // setProfilePicUrl(res.data.profileImageUrl);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [songs] = useState(songsjson.arr);

  return (
    <div>
      {isDataLoaded && isArrayEmpty ? (
        <>
          <Player
            currentSongIndex={currentSongIndex}
            setCurrentSongIndex={setCurrentSongIndex}
            nextSongIndex={nextSongIndex}
            songs={songs}
          />
        </>
      ) : (
        <>
          Its's Empty Here. Try Adding Some Tracks.
          <div>
            <Link to='/SongUpload'>
              <Button variant='light' className='btn-sm'>
                Upload a New Song
              </Button>
            </Link>
          </div>
        </>
      )}
      <div className='table-responsive' style={{ marginTop: "5vh" }}>
        <table className='table table-striped table-hover text-nowrap mb-0'>
          <thead className='thead-light'>
            <tr>
              <th style={{ width: "40%" }}>Songs</th>
              {/* <th className="text-center">Status</th>
                                            <th className="text-center">Actions</th> */}
            </tr>
          </thead>
          <tbody>
            {songData.map((song) => (
              <tr key={song._id}>
                <td>
                  <div className='d-flex align-items-center'>
                    <Avatar
                      alt='...'
                      src='/assets/download.png'
                      className='mr-2'
                    />

                    <div>
                      <a
                        href='#/'
                        onClick={(e) => e.preventDefault()}
                        className='font-weight-bold text-black'
                        title='...'
                      >
                        {song.title}
                      </a>
                      <span className='text-black-50 d-block'>
                        {song.artist[0]}
                      </span>
                    </div>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default MusicPlayer;
