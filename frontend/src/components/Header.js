import React, { useEffect } from "react";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";

const Header = () => {
  let userName = "";
  const logoutHandler = () => {
    //   localStorage.removeItem("token");
    localStorage.clear();
    window.location.href = "/";
  };
  useEffect(() => {
    console.log(userName);
  }, []);
  userName = localStorage.getItem("userName");

  return (
    <header>
      <Navbar bg='primary' variant='dark' expand='lg' collapseOnSelect>
        <Container>
          <Link to='/musicplayer'>
            <Navbar.Brand>MUSIC IS LIFE</Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='justify-content-end' style={{ width: "100%" }}>
              {userName ? (
                <NavDropdown title={userName} id='username'>
                  {/* <Link to='/musicplayer'>
                    <NavDropdown.Item>Profile</NavDropdown.Item>
                  </Link> */}
                  <NavDropdown.Item onClick={logoutHandler}>
                    Logout
                  </NavDropdown.Item>
                </NavDropdown>
              ) : (
                <Nav.Link href='/login'>Login</Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
