import React from "react";
import { Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { Link } from "react-router-dom";
import "./LandingPage.css";

const LandingPage = () => {
  return (
    <>
      <main className='mainclass'>
        <div class='jumbotron'>
          <div class='containerclass'>
            <h1>MUSIC PLAYER </h1>
            <Link to='/login'>
              <Button variant='light' className='btn-lg'>
                LOGIN TO CONTINUE
              </Button>
            </Link>
          </div>
        </div>
      </main>

      <footer>
        <div class='containerclass'>
          <p>&copy; Ajatus 2021</p>
        </div>
      </footer>
    </>
  );
};

export default LandingPage;
