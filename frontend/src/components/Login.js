import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Form, Button, Row, Col } from "react-bootstrap";
import FormContainer from "./FormContainer";
import { environment } from "../environment.js";
import axios from "axios";
import swal from "sweetalert";

const LoginScreen = ({ history, location }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  let token;
  let refreshToken;
  let userDetails;

  let userInfo = "";
  let userName = "";

  const redirect = location.search ? location.search.split("=")[1] : "/";

  useEffect(() => {
    if (!userInfo) {
      history.push("/login");
    }
  }, [history]);

  const submitHandler = (e) => {
    e.preventDefault();
    login(email, password);
  };

  const login = (email, password) => {
    axios
      .post(environment.apiUrl + "api/users/login", {
        email: email,
        password: password,
      })
      //   .then((res) => {
      //     console.log(res);
      //     userInfo = res.data.userId;
      //     userName = res.data.userName;
      //     localStorage.setItem("userInfo", userInfo);
      //     localStorage.setItem("userName", userName);
      //     history.push("/musicplayer");
      //   })
      .then((res) => {
        console.log(res);
        userName = res.data.userName;
        localStorage.setItem("userName", userName);
        localStorage.setItem("userInfo", JSON.stringify(res.data));

        if (res?.data?._id) {
          localStorage.setItem("userInfo", JSON.stringify(res.data));
          window.location.href = "/musicplayer";
        } else {
          swal({
            title: "Error",
            text: res?.data?.message,
            icon: "error",
            dangerMode: true,
          });
        }
      })
      .catch((err) => {
        // setShowLoading(false);
        swal({
          title: "Error",
          text: "Something Wrong Happened",
          icon: "error",
          dangerMode: true,
        });
        console.log(err);
      });
  };
  return (
    <FormContainer>
      <h1>Sign In</h1>

      <Form onSubmit={submitHandler}>
        <Form.Group controlId='email'>
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type='email'
            placeholder='Enter email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>

        <Form.Group controlId='password'>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type='password'
            placeholder='Enter password'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Button type='submit' variant='primary' style={{ marginTop: "10px" }}>
          Sign In
        </Button>
      </Form>

      <Row className='py-3'>
        <Col>
          Don't have an account yet?
          <Link to={redirect ? `/register?redirect=${redirect}` : "/register"}>
            Register
          </Link>
        </Col>
      </Row>
    </FormContainer>
  );
};

export default LoginScreen;
