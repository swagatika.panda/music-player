import React, { useState } from "react";
import axios from "axios";

//import { environment } from "./environment";

const SongUpload = () => {
  const [selectedFile, setSelectedFile] = useState("");

  const [message, setMessage] = useState("");

  const showFile = async (e) => {
    e.preventDefault();
    const reader = new FileReader();
    reader.onload = async (e) => {
      const text = e.target.result;
      console.log(text);
      alert(text);
    };
    reader.readAsText(e.target.files[0]);
  };

  const handleSubmission = async () => {
    const formData = new FormData();

    formData.append("userPhoto", selectedFile);
    await axios
      .post("http://localhost:5000/fileUploadAPI/uploadFile", formData)
      .then((res) => {
        console.log(res.data.success);
        window.alert("Success");
        console.log();
        //handleUpdate();
      })
      .catch((err) => {
        console.log(err);
        window.alert("Error");
      });
  };

  const changeHandler = async (event) => {
    console.log(event.target.files[0].name);
    await setSelectedFile(event.target.files[0]);
    await setTimeout(function () {
      console.log(selectedFile);
    }, 9000);
  };

  return (
    <div>
      <div>
        <div className='row'>
          <div className='col-lg-3'></div>
          <div className='col-6'>
            <div className='card' style={{ marginTop: "10%" }}>
              <div className='card-body'>
                <div className='border-bottom  pb-4'>
                  <div className='text-center'>
                    <img
                      //src='../assets/audio.jpg'
                      src='https://ieltssharebox.com/wp-content/uploads/2019/03/musicworkshop-image.jpg'
                      alt='audio'
                      style={{ width: "50%", height: "50%" }}
                      className='img-lg rounded-circle mb-3'
                    />
                    <div className='mb-3'>
                      <div className='d-flex  justify-content-center'>
                        <input onChange={changeHandler} type='file' />
                      </div>
                      <button
                        className='btn btn-primary'
                        onClick={handleSubmission}
                        style={{ marginTop: "10px" }}
                      >
                        UPLOAD
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SongUpload;
