import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";

import LoginScreen from "./components/Login.js";
import RegisterScreen from "./components/Register.js";
import MusicPlayer from "./components/MusicPlayer/MusicPlayer";
import LandingPage from "./components/LandingPage";
import uploadSong from "./components/uploadSong";
import Header from "./components/Header";

function App() {
  let userData = JSON.parse(localStorage.getItem("userInfo"));

  return (
    <>
      {userData ? (
        <>
          <Router>
            <Header />
            <Container>
              <Switch>
                {/* <Redirect to='/musicplayer' /> */}
                <Route path='/musicplayer' component={MusicPlayer} />

                <Route path='/SongUpload' component={uploadSong} />
              </Switch>
            </Container>
          </Router>
        </>
      ) : (
        <>
          <Router>
            <Route path='/login' component={LoginScreen} />
            <Route path='/register' component={RegisterScreen} />
            <Route path='/' component={LandingPage} exact />
          </Router>
        </>
      )}
    </>
  );
}

export default App;
