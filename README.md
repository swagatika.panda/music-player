## Available Scripts

## Clone the repo

Open your terminal and then type

### `git clone {the url to the GitHub repo}`

This clones the repo

## Run the backend

cd into the new folder and type

### `cd music-player`

### `$ npm install`

This installs the required dependencies To run the nodejs app.

In the project directory, you can run:

### `npm start`

Runs the backend in the development mode.\
Open [http://localhost:5000]

## Run the frontend

cd into frontend to run the react application

### `cd frontend`

In the project directory, you can run:

### `npm start`

Runs the frontend in the development mode.\
Open [http://localhost:3000]
