const mongoose = require("mongoose");
const dbConfig = require("./db.config.js");

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(dbConfig.url, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });

    console.log(`MongoDB Connected ${conn.connection.host}`);
  } catch (error) {
    console.error(`Error: ${error.message}`);
    process.exit(1);
  }
};

module.exports = connectDB;
