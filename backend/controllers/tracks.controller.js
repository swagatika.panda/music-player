const Track = require("../models/tracks.model");

exports.findTracksByUserId = (req, res) => {
  Track.find({ userId: req.params.userId })
    .then((track) => {
      console.log(track);
      if (!track) {
        return res.status(404).send({
          message: "Note not found with id " + req.params._id,
        });
      }
      res.send(track);
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Note not found with id " + req.params._id,
        });
      }
      return res.status(500).send({
        message: "Error retrieving note with id " + req.params._id,
      });
    });
};

exports.create = (req, res) => {
  const track = new Track({
    url: req.body.url,
    cover: req.body.cover,
    title: req.body.title,
    artist: req.body.artist,
    userId: req.body.userId,
    isActive: true,
    isDeleted: false,
  });

  // Save Note in the database
  track
    .save()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Note.",
      });
    });
};
