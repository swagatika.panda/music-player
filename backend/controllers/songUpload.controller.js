var multer = require("multer");
var imageName;

exports.uploadSong = (req, res) => {
  var storage = multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, "./uploads");
    },
    filename: function (req, file, callback) {
      imageName = Date.now() + file.originalname.replace(/\s+/g, "-");
      callback(null, imageName);
      console.log(file);
    },
  });
  var upload = multer({ storage: storage }).single("userTrack");

  const function = (req, res) => {
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
        return res.json({ error: "Image Upload Failed" });
      }
      console.log(imageName);
      res.json({ success: imageName });
    });
    
    }

};
