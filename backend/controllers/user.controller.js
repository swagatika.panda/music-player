const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.register = (req, res, next) => {
  bcrypt.hash(req.body.password, 10).then((hash) => {
    const user = new User({
      userName: req.body.userName,
      email: req.body.email,
      // password: req.body.password,
      isActive: true,
      isDeleted: false,
      password: hash,
    });
    user
      .save()
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        res.status(500).json({
          error: error,
        });
      });
  });
};

exports.login = (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          error: new Error("user not found"),
        });
      }
      bcrypt
        .compare(req.body.password, user.password)
        .then((valid, data) => {
          if (!valid) {
            return res.status(401).json({
              error: new Error("Incorrect password"),
            });
          }
          const token = jwt.sign({ userId: user._id }, "RANDOM_TOKEN_SECRET", {
            expiresIn: "24h",
          });
          //   res.status(200).json({
          //     userName: user.userName,
          //     userId: user._id,
          //     token: "token",
          //   });
          res.send(user);
        })
        .catch((errro) => {
          res.status(500).json({
            error: error,
          });
        });
    })
    .catch((error) => {
      res.status(500).json({
        error: error,
      });
    });
};

// exports.login = (req, res) => {
//   User.findOne({ email: req.body.email })
//     .then((data) => {
//       if (!data) {
//         res.send({ message: "Email Not Found" });
//       } else {
//         if (req.body.password == data.password) {
//           res.send(data);
//         } else {
//           res.send({ message: "Incorrect Password" });
//         }
//       }
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while creating the Note.",
//       });
//     });
// };
