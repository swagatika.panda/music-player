const express = require("express");
const router = express.Router();
const songCtrl = require("../controllers/tracks.controller.js");

router.post("/songsApi/createTrackForUser", songCtrl.create);
router.get("/songsApi/getTracksById/:userId", songCtrl.findTracksByUserId);

module.exports = router;
