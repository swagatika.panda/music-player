var multer = require("multer");
var fileName;
module.exports = (app) => {
  var storage = multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, "./uploads");
    },
    filename: function (req, file, callback) {
      fileName = Date.now() + file.originalname.replace(/\s+/g, "-");
      callback(null, fileName);
      console.log(file);
    },
  });
  var upload = multer({ storage: storage }).single("userPhoto");
  app.post("/fileUploadAPI/uploadFile", function (req, res) {
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
        return res.json({ error: "file Upload Failed" });
      }
      console.log(fileName);
      res.json({ success: fileName });
    });
  });
};
