const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
  userName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  isActive: { type: Boolean },
  isActive: { type: Boolean },
});

userSchema.plugin(uniqueValidator);
//this plugin ensures that no two user use the same email id

module.exports = mongoose.model("User", userSchema);
