const mongoose = require("mongoose");

const TrackSchema = mongoose.Schema(
  {
    url: String,
    cover: String,
    title: String,
    artist: Array,
    userId: String,
    isActive: Boolean,
    isDeleted: Boolean,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("tracks", TrackSchema);
