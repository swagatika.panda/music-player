const express = require("express");
const mongoose = require("mongoose");
const app = express();
const dotenv = require("dotenv");
const connectDB = require("./config/db.js");
const bodyParser = require("body-parser");
var cors = require("cors");
const userRoutes = require("./routes/user.route.js");
const fileUpload = require("./routes/fileUploadRoutes/fileUpload.js");
const songsRoutes = require("./routes/tracks.route.js");

app.use(express.json());
app.use(cors());

connectDB();

// require("./routes/FileUploadRoute")(app);
// // parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }));

// app.get("/:fileName", (req, res) => {
//   res.sendFile("uploads/" + req.params.fileName, { root: "." });
//   // res.sendFile("" + req.params.fileName);
// });

app.get("/:imageName", (req, res) => {
  res.sendFile("uploads/" + req.params.imageName, { root: "." });
  // res.sendFile("" + req.params.imageName);
});
// require("./routes/fileUploadRoutes/fileUpload")(app);

app.use("/api/users", userRoutes);
require("./routes/fileUploadRoutes/fileUpload.js")(app);
app.use("/api/songs", songsRoutes);

app.get("/", (req, res) => {
  res.send("server running ");
});

const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(`server running in ${process.env.NODE_ENV} mode on ${PORT}`)
);
